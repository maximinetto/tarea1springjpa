package com.maximiliano.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maximiliano.exception.ModeloNotFoundException;
import com.maximiliano.model.Persona;
import com.maximiliano.service.PersonaService;

@RestController
@RequestMapping("/personas")
public class PersonaController {
	
	@Autowired
	private PersonaService service;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Resources<Persona> listar(){
		List<Persona> listaPersonas = service.listar();
		listaPersonas.forEach(persona -> {
			Link selfLink = linkTo(methodOn(this.getClass()).listarPorId(persona.getIdPersona())).withSelfRel();
			persona.add(selfLink);
		});
		
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listar());
		return new Resources<>(listaPersonas,linkTo.withRel("personas-resource"));
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Resource<Persona> listarPorId(@PathVariable("id") Long id) {
		Persona persona = service.listarPorId(id);
		if(persona == null) {
			throw new ModeloNotFoundException("Id no encontrado: " + id);
		}
		Resource<Persona> resource = new Resource<>(persona);
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarPorId(id));
		resource.add(linkTo.withRel("persona-resource"));
		return resource;
	}
	
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registrar(@Valid @RequestBody Persona persona)
	{
		Persona per = service.regitrar(persona);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(per.getIdPersona()).toUri();
		return ResponseEntity.created(location).build();		
	}
	
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> modificar(@Valid @RequestBody Persona persona) {
		service.modificar(persona);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Long id) {
		Persona persona = service.listarPorId(id);
		if(persona == null) {
			throw new ModeloNotFoundException("Id no encontrado: " + id);
		}
		service.eliminar(id);
	}
	
	/*private LongitudCaracteresException devolverExcepcion(Persona persona)
	{
		int longitudNombre = persona.getNombres().length();
		boolean longitudValidaNombres = longitudNombre >= 3 && longitudNombre <= 70;
		if(!longitudValidaNombres)
			return new LongitudCaracteresException("Nombres debe ser de 3 a 70 caracteres");
		
		int longitudApellidos = persona.getApellidos().length();
		boolean longitudValidaApellidos = persona.getApellidos() != null && longitudApellidos >= 3 &&
				longitudApellidos <= 70;
		if(!longitudValidaApellidos) 
			return new LongitudCaracteresException("Apellidos debe ser de 3 a 70 caracteres");
		
		return null;
				
	}
	
	private CampoVacioException devolverExceptionVacios(Persona persona) {
		if(persona.getNombres() == null)
			return new CampoVacioException("Nombres no debe ser vacio");
		
		if(persona.getApellidos() == null)
			return new CampoVacioException("Apellidos no debe ser vacio");
		
		return null;
	}*/
}
