package com.maximiliano.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maximiliano.exception.ModeloNotFoundException;
import com.maximiliano.model.Venta;
import com.maximiliano.service.VentaService;

@RestController
@RequestMapping("/ventas")
public class VentaController {
	
	@Autowired
	private VentaService ventaService;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Resources<Venta> listar(){
		List<Venta> listaVentas = ventaService.listar();
		listaVentas.forEach(venta -> {
			Link selfLink = linkTo(methodOn(this.getClass()).listarPorId(venta.getIdVenta())).withSelfRel();
			venta.add(selfLink);
		});
		
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listar());
		return new Resources<>(listaVentas,linkTo.withRel("ventas-resource"));
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Resource<Venta> listarPorId(@PathVariable("id") Long id) {
		Venta venta = ventaService.listarPorId(id);
		if(venta == null) {
			throw new ModeloNotFoundException("Id no encontrado: " + id);
		}
		Resource<Venta> resource = new Resource<>(venta);
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarPorId(id));
		resource.add(linkTo.withRel("venta-resource"));
		return resource;
	}
	
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registrar(@Valid @RequestBody Venta venta)
	{
		Venta ven = ventaService.regitrar(venta);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(ven.getIdVenta()).toUri();
		return ResponseEntity.created(location).build();		
	}
}
