package com.maximiliano.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maximiliano.exception.ModeloNotFoundException;
import com.maximiliano.model.Producto;
import com.maximiliano.service.ProductoService;

@RestController
@RequestMapping("/productos")
public class ProductoController {

	@Autowired
	private ProductoService productoService;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Resources<Producto> listar(){
		List<Producto> listaProductos = productoService.listar();
		listaProductos.forEach(producto -> {
			Link selfLink = linkTo(methodOn(this.getClass()).listarPorId(producto.getIdProducto())).withSelfRel();
			producto.add(selfLink);
		});
		
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listar());
		return new Resources<>(listaProductos, linkTo.withRel("productos-resource"));
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Resource<Producto> listarPorId(@PathVariable("id") Integer id) {
		Producto producto = productoService.listarPorId(id);
		if(producto == null) {
			throw new ModeloNotFoundException("Id no encontrado: " + id);
		}
		
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarPorId(producto.getIdProducto()));
		return new Resource<>(producto,linkTo.withRel("producto-resource"));
	}
	
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registrar(@Valid @RequestBody Producto producto)
	{
		Producto prod = productoService.regitrar(producto);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{id}")
				.buildAndExpand(prod.getIdProducto()).toUri();
		return ResponseEntity.created(location).build();		
	}
	
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> modificar(@Valid @RequestBody Producto producto) {
		productoService.modificar(producto);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id) {
		Producto producto = productoService.listarPorId(id);
		if(producto == null) {
			throw new ModeloNotFoundException("Id no encontrado: " + id);
		}
		productoService.eliminar(id);
	}
}
