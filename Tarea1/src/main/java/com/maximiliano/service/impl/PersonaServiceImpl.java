package com.maximiliano.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maximiliano.dao.PersonaDAO;
import com.maximiliano.model.Persona;
import com.maximiliano.service.PersonaService;

@Service
public class PersonaServiceImpl implements PersonaService{
	
	@Autowired	
	private PersonaDAO personaDAO;
	
	@Override
	public List<Persona> listar() {
		
		return personaDAO.findAll();
	}

	@Override
	public Persona listarPorId(Long id) {
		
		return personaDAO.findOne(id);
	}

	@Override
	public Persona regitrar(Persona t) {
		return personaDAO.save(t);
	}

	@Override
	public Persona modificar(Persona t) {
		return personaDAO.save(t);
	}

	@Override
	public void eliminar(Long id) {
		personaDAO.delete(id);
	}

}
