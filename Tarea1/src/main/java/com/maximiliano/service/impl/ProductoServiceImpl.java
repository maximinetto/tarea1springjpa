package com.maximiliano.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maximiliano.dao.ProductoDAO;
import com.maximiliano.model.Producto;
import com.maximiliano.service.ProductoService;

@Service
public class ProductoServiceImpl implements ProductoService{

	@Autowired
	private ProductoDAO productoDAO;
	
	@Override
	public List<Producto> listar() {
		return productoDAO.findAll();
	}

	@Override
	public Producto listarPorId(Integer id) {
		return productoDAO.findOne(id);
	}

	@Override
	public Producto regitrar(Producto t) {
		return productoDAO.save(t);
	}

	@Override
	public Producto modificar(Producto t) {
		return productoDAO.save(t);
	}

	@Override
	public void eliminar(Integer id) {
		productoDAO.delete((int)id);
	}

}
