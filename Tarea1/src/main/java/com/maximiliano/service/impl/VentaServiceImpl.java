package com.maximiliano.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maximiliano.dao.VentaDAO;
import com.maximiliano.model.Venta;
import com.maximiliano.service.VentaService;

@Service
public class VentaServiceImpl implements VentaService{

	@Autowired
	private VentaDAO ventaDAO;
	
	@Override
	public List<Venta> listar() {
		return ventaDAO.findAll(); 
	}

	@Override
	public Venta listarPorId(Long id) {
		return ventaDAO.findOne(id);
	}

	@Override
	public Venta regitrar(Venta venta) {
		venta.getDetalleVenta().forEach(detalle -> detalle.setVenta(venta));
		return ventaDAO.save(venta);
	}

	@Override
	public Venta modificar(Venta t) {
		return ventaDAO.save(t);
	}

	@Override
	public void eliminar(Long id) {
		ventaDAO.delete(id);
	}

}
