package com.maximiliano.service;

import java.util.List;

public interface CRUD<T,S> {

	List<T> listar();
	T listarPorId(S id);
	T regitrar(T t);
	T modificar(T t);
	void eliminar(S id);
	
}
