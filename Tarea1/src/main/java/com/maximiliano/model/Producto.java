package com.maximiliano.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.hateoas.ResourceSupport;


@Entity
@Table(name = "producto")
public class Producto extends ResourceSupport{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idProducto; 
	
	@NotNull(message = "El nombre del producto es obligatorio")
	@Size(min = 2, max = 50, message = "El nombre del producto debe estar entre los 2 a 50 caracteres")
	@Column(name = "nombre", nullable = false, length = 50)
	private String nombre; 
	
	@NotNull(message = "La marca del producto es obligatorio")
	@Size(min = 2, max = 50, message = "La marca del producto debe estar entre los 2 a 50 caracteres")
	@Column(name = "marca", nullable = false, length = 50)
	private String marca;
	
	public Producto() {
		
	}
	
	public Producto(Integer idProducto, String nombre, String marca) {
		super();
		this.idProducto = idProducto;
		this.nombre = nombre;
		this.marca = marca;
	}
	
	public Integer getIdProducto() {
		return idProducto;
	}
	
	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getMarca() {
		return marca;
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}
		
}
