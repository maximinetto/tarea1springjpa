package com.maximiliano.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.hateoas.ResourceSupport;

@Entity
@Table(name = "persona")
public class Persona extends ResourceSupport{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idPersona;
	
	@NotNull
	@Size(min = 3 , max = 70,message = "Nombres debe ser de 3 a 70 caracteres")
	@Column(name = "nombres", nullable = false, length = 70)
	private String nombres;
	
	@NotNull
	@Size(min = 3, max = 70, message = "Apellidos debe ser de 3 a 70 caracteres")
	@Column(name = "apellidos", nullable = false, length = 70)
	private String apellidos;
	
	public Long getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	
}
