package com.maximiliano.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maximiliano.model.Producto;

public interface ProductoDAO extends JpaRepository<Producto,Integer>{
	
}
