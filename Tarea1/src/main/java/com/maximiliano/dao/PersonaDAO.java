package com.maximiliano.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maximiliano.model.Persona;

public interface PersonaDAO extends JpaRepository<Persona, Long> {

}
