package com.maximiliano.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maximiliano.model.Venta;

public interface VentaDAO extends JpaRepository<Venta,Long>{

}
