package com.maximiliano.exception;

public class LongitudCaracteresException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LongitudCaracteresException(String mensaje) {
		super(mensaje);
	}
	
}
