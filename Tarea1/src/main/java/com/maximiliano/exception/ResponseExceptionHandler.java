package com.maximiliano.exception;

import java.util.Date;
import java.util.ListIterator;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler{

	@ExceptionHandler(ModeloNotFoundException.class)
	public final ResponseEntity<Object> manejarModeloExcepciones(ModeloNotFoundException modeloNotFoundException,
			WebRequest request){
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), modeloNotFoundException.getMessage(),
				request.getDescription(false));
		return new ResponseEntity<>(exceptionResponse, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(LongitudCaracteresException.class)
	public final ResponseEntity<Object> manejoLongitudExcepciones(LongitudCaracteresException longitudCaracteresException,
			WebRequest request){
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(),longitudCaracteresException.getMessage(),
				request.getDescription(false));
		return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(CampoVacioException.class)
	public final ResponseEntity<Object> manejoCamposVaciosExcepciones(CampoVacioException campoVacioException,
			WebRequest request){
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(),campoVacioException.getMessage(),
				request.getDescription(false));
		return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		String errores = "";
		ListIterator<ObjectError> errorsListIterator = ex.getBindingResult().getAllErrors().listIterator();
		while(errorsListIterator.hasNext()) {
			ObjectError objectError = errorsListIterator.next();
			errores += objectError.getDefaultMessage() + ", ";
			
			if(errorsListIterator.nextIndex() == ex.getBindingResult().getAllErrors().size())
				errores += objectError.getDefaultMessage();
		}
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), "Validación Fallida", errores);
		return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}
	
	
}
