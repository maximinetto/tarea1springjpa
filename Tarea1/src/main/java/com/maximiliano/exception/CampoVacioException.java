package com.maximiliano.exception;

public class CampoVacioException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CampoVacioException(String mensaje) {
		super(mensaje);
	}
}
